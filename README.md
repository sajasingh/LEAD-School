# Project Running Instructions:

1. DOWNLOAD the repository as ZIP file (It will be downloaded as LEAD-School-master.zip)
2. EXTRACT the LEAD-School-master Zip File (After extracting we can see 2 files - 1.LEAD-School and 2.README.md)
3. OPEN the Eclipse IDE
4. GO TO File Menu in ECLIPSE and click IMPORT
8. SELECT Maven and DOUBLE-Click
9. SELECT Existing Maven Project and CLICK Next button
10. CLICK Browse and got the DIRECTORY
11. SELECT LEAD-School folder from LEAD-SCHOOL-master and CLICK OK
12. CLICk Finish
12. RIGHT-CLICK on the project and CLICK Run As - Maven Clean
13. RIGHT-CLICK on the project and CLICK on Maven - Update Project
14. RIGHT-CLICK on the TestAmazon.java inside com.amazon.tests - Rus As - TestNG Test
15. For any doubts plz refer > 
Detailed Steps of Test FrameWork: in the mail or plz feel free to contact me